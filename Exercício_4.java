public class Calculadora {
    // Método para calcular a soma
    public double soma(double num1, double num2) {
        return num1 + num2;
    }

    // Método para calcular a subtração
    public double subtracao(double num1, double num2) {
        return num1 - num2;
    }

    // Método para calcular a multiplicação
    public double multiplicacao(double num1, double num2) {
        return num1 * num2;
    }

    // Método para calcular a radiciação
    public double radiciacao(double num, int indice) {
        return Math.pow(num, 1.0 / indice);
    }

    // Método para calcular o fatorial
    public int fatorial(int num) {
        if (num == 0) {
            return 1;
        } else {
            return num * fatorial(num - 1);
        }
    }

    public static void main(String[] args) {
        Calculadora calc = new Calculadora();

        // Exemplos de uso
        System.out.println("Soma: " + calc.soma(5, 3));
        System.out.println("Subtração: " + calc.subtracao(5, 3));
        System.out.println("Multiplicação: " + calc.multiplicacao(5, 3));
        System.out.println("Radiciação: " + calc.radiciacao(9, 2));
        System.out.println("Fatorial de 5: " + calc.fatorial(5));
    }
}

/*Neste código, a classe Calculadora possui métodos para realizar as operações de 
soma, subtração, multiplicação, radiciação e cálculo de fatorial. O cálculo do fatorial é
feito de forma recursiva. No método main, são realizados exemplos de uso desses métodos. */