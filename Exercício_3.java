import java.util.Arrays;

public class CalculadoraFormas {
    // Retorna a área do círculo
    public double calcular(double raio) {
        return Math.PI * raio * raio;
    }

    // Retorna a área do quadrado
    public double calcular(double lado1, double lado2) {
        return lado1 * lado2;
    }

    // Retorna o perímetro do triângulo
    public int calcular(int lado1, int lado2, int lado3) {
        return lado1 + lado2 + lado3;
    }

    // Retorna a área do triângulo
    public double calcular(int base, double altura) {
        return 0.5 * base * altura;
    }

    // Retorna a área do triângulo
    public double calcular(double[] x, double[] y) {
        double area = 0.0;
        for (int i = 0; i < x.length; i++) {
            area += x[i] * y[(i + 1) % x.length] - y[i] * x[(i + 1) % x.length];
        }
        return Math.abs(area) / 2.0;
    }

    // Retorna o texto "Uso incorreto"
    public String calcular(String input) {
        return "Uso incorreto";
    }

    public static void main(String[] args) {
        CalculadoraFormas calculadora = new CalculadoraFormas();

        // Exemplos de uso
        System.out.println("Área do círculo: " + calculadora.calcular(5.0));
        System.out.println("Área do quadrado: " + calculadora.calcular(4.0, 4.0));
        System.out.println("Perímetro do triângulo: " + calculadora.calcular(3, 4, 5));
        System.out.println("Área do triângulo: " + calculadora.calcular(4, 6.0));
        System.out.println("Área do triângulo (coordenadas): " + calculadora.calcular(new double[]{0, 3, 6}, new double[]{0, 4, 0}));
        System.out.println("Uso incorreto: " + calculadora.calcular("teste"));
    }
}

/*Neste exercício, a classe CalculadoraFormas possui vários métodos calcular, cada um com 
diferentes tipos e quantidades de parâmetros. O método a ser chamado é determinado pelo 
número e tipo de parâmetros passados para ele. A implementação dos cálculos é simples para 
ilustrar o conceito de sobrecarga de métodos.*/