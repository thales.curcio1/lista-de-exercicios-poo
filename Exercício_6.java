import java.util.regex.*;

public class Cliente {
    private String nome;
    private String endereco;
    private String cep;
    private String cpf;

    public Cliente(String nome, String endereco, String cep, String cpf) {
        this.nome = nome;
        this.endereco = endereco;
        this.cep = cep;
        this.cpf = cpf;
    }

    // Métodos de acesso para os atributos
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    // Método para validar CPF
    public boolean validarCPF(String cpf) {
        String regex = "\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}";
        return Pattern.matches(regex, cpf);
    }

    // Método para validar CEP
    public boolean validarCEP(String cep) {
        String regex = "\\d{5}-\\d{3}";
        return Pattern.matches(regex, cep);
    }

    // Método para exibir os dados do cliente
    public void exibirDadosCliente() {
        System.out.println("Nome: " + nome);
        System.out.println("Endereço: " + endereco);
        System.out.println("CEP: " + cep);
        System.out.println("CPF: " + cpf);
    }

    public static void main(String[] args) {
        // Exemplo de uso
        Cliente cliente1 = new Cliente("Fulaninho", "Rua A, 123", "12345-678", "123.456.789-00");

        // Exibir dados do cliente
        System.out.println("Dados do Cliente:");
        cliente1.exibirDadosCliente();

        // Validar CPF e CEP
        if (cliente1.validarCPF(cliente1.getCpf())) {
            System.out.println("CPF válido");
        } else {
            System.out.println("CPF inválido");
        }

        if (cliente1.validarCEP(cliente1.getCep())) {
            System.out.println("CEP válido");
        } else {
            System.out.println("CEP inválido");
        }
    }
}
