public class CalculadoraPrimos {
    public int contaPrimos(int inicio, int fim) {
        int count = 0;
        for (int i = inicio; i <= fim; i++) {
            if (ehPrimo(i)) {
                count++;
            }
        }
        return count;
    }

    // Método auxiliar para verificar se um número é primo
    private boolean ehPrimo(int num) {
        if (num <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        CalculadoraPrimos calculadora = new CalculadoraPrimos();
        int inicio = 5;
        int fim = 20;
        int quantidadePrimos = calculadora.contaPrimos(inicio, fim);
        System.out.println("De " + inicio + " até " + fim + " existem " + quantidadePrimos + " números primos.");
    }
}

/*Neste exercício, a classe CalculadoraPrimos possui um método contaPrimos() que recebe dois 
números inteiros como parâmetros e retorna a quantidade de números primos encontrados entre 
esses dois números (inclusive). O método ehPrimo() é um método auxiliar usado para verificar 
se um número é primo ou não. No método main, um objeto CalculadoraPrimos é criado e o 
método contaPrimos() é chamado com os valores 5 e 20 como parâmetros. O resultado é impresso 
no console. */