import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class DiferencaEntreDatas {
    public static void main(String[] args) {
        // Defina as datas
        LocalDate data1 = LocalDate.of(2023, 3, 1);
        LocalDate data2 = LocalDate.of(2024, 3, 1);

        // Calcule a diferença entre as datas
        long diferencaDias = calcularDiferenca(data1, data2);

        // Exiba o resultado
        System.out.println("A diferença entre as datas é de " + diferencaDias + " dias.");
    }

    public static long calcularDiferenca(LocalDate data1, LocalDate data2) {
        // Use ChronoUnit para calcular a diferença entre as datas
        return ChronoUnit.DAYS.between(data1, data2);
    }
}
/*Neste exemplo, usamos a classe LocalDate para representar as datas data1 e data2. 
Em seguida, usamos o método calcularDiferenca para calcular a diferença entre essas datas, 
usando a classe ChronoUnit para calcular a diferença em dias. O resultado é então impresso no 
console. */