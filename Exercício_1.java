import java.util.regex.*;

class Cliente {
    private String nome;
    private String endereco;
    private String cep;
    private String cpf;

    public Cliente(String nome, String endereco, String cep, String cpf) {
        this.nome = nome;
        this.endereco = endereco;
        this.cep = cep;
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public boolean validarCPF(String cpf) {
        // Expressão regular para validar CPF formato ###.###.###-##
        String regex = "\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}";
        return Pattern.matches(regex, cpf);
    }

    public boolean validarCEP(String cep) {
        // Expressão regular para validar CEP formato #####-###
        String regex = "\\d{5}-\\d{3}";
        return Pattern.matches(regex, cep);
    }

    @Override
    public String toString() {
        return "Nome: " + nome + "\nEndereço: " + endereco + "\nCEP: " + cep + "\nCPF: " + cpf;
    }
}

public class CadastroClientes {
    public static void main(String[] args) {
        // Exemplo de utilização
        Cliente cliente1 = new Cliente("Fulaninho", "Rua A, 123", "12345-678", "123.456.789-00");

        // Exibir dados do cliente cadastrado
        System.out.println("Dados do Cliente:");
        System.out.println(cliente1);

        // Validar CPF e CEP
        if (cliente1.validarCPF(cliente1.getCpf())) {
            System.out.println("CPF válido");
        } else {
            System.out.println("CPF inválido");
        }

        if (cliente1.validarCEP(cliente1.getCep())) {
            System.out.println("CEP válido");
        } else {
            System.out.println("CEP inválido");
        }
    }
}

/*Este código cria uma classe Cliente com os atributos nome, endereço, CEP e CPF. 
Os métodos get e set são usados para acessar e modificar esses atributos de forma encapsulada. 
Além disso, há métodos para validar o CPF e o CEP usando expressões regulares. 
O programa principal CadastroClientes cria um cliente de exemplo, 
exibe seus dados e valida o CPF e o CEP. */